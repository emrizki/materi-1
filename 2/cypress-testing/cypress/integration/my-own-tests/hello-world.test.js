/// <reference types="cypress" />

describe('Basic Tests', () => {
  it('we have correct page title', () => {
    cy.visit('https://privy.id');
  });

  it('Login page looks good', () => {
    cy.viewport(1280, 720);
    cy.visit('https://privy.id');

    cy.contains('Login').click({ force: true });
    cy.contains('Log In').should('exist');
    cy.contains('Forgot your PrivyID or Password?').should('exist');
    cy.contains('Log In with').should('exist');
    cy.contains('QR Code').should('exist');
  });

  it('The login page links work', () => {
    cy.viewport(1280, 720);
    cy.visit('https://privy.id');

    cy.contains('Login').click({ force: true });

    cy.log('Going to Forgot PrivyID or Password');
    cy.contains('Forgot your PrivyID or Password?').click();
    cy.url().should('include', '/password/new');

    cy.url().then((value) => {
      cy.log('The current real URL is: ', value);
    });

    cy.go('back');

    cy.contains('QR Code').click();
    cy.url().should('include', '/login?method=qr');

    cy.go('back');
  });

  it('Login should work fine', () => {
    cy.viewport(1280, 720);
    cy.visit('https://privy.id');

    cy.contains('Login').click({ force: true });
    cy.wait(2000);
    cy.get('input[placeholder="PrivyID"]').type('XK0674');
    cy.contains('CONTINUE').click();
    cy.get('[placeholder=Password]').type('PrivyId96!');
    cy.contains('CONTINUE').click();
  });
});
