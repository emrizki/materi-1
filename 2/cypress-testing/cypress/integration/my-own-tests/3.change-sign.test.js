/// <reference types="cypress" />

describe('Change Image Signature', () => {
  it('Change image signature', () => {
    cy.viewport(1280, 720);
    cy.visit('https://privy.id');

    cy.loginTest();

    cy.contains('Change My Signature Image').click();
    cy.contains('Add Signature').click({ force: true });

    cy.contains('Image').click();
    const imageSign = 'mrizki_signature.png';

    cy.wait(2000);
    cy.get('.setting-sign > div > fieldset:first-child').click();
    cy.get('div > input[type="file"]').attachFile(imageSign);
    cy.contains('Crop').click();
    cy.contains('Apply').click();
    cy.get('.setting-sign > div > fieldset:last-child').click();
    cy.get('div > input[type="file"]').attachFile(imageSign);
    cy.contains('Crop').click();
    cy.contains('Apply').click();
    cy.contains('Save').click();
    cy.wait(5000);
    cy.visit('https://app.privy.id/dashboard');
    cy.contains('Change My Signature Image').click();
    cy.wait(5000);
    cy.get(
      '.signature-item:first-child > .signature-item__control > button:first-child'
    ).click({
      force: true,
    });
  });
});
