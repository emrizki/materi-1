/// <reference types="cypress" />

const baseUrl = 'https://privy.id';

describe('Self Sign Feature', () => {
  it.only('Self Sign', () => {
    cy.viewport(1280, 720);
    cy.visit(baseUrl);

    cy.loginTest();

    cy.get('button#v-step-0').click();
    cy.contains('Self Sign').click();

    const fileName = 'mrizki_appletter.pdf';
    cy.get('.form-upload > input[type="file"]').attachFile({
      filePath: fileName,
      encoding: 'base64',
    });

    cy.get('.modal-footer > .btn-danger').click();
    cy.wait(1000);
    cy.contains('Continue').click();
    cy.wait(1000);
    cy.get('#step-document-1').click();
    cy.contains('Done').click();
    cy.wait(4000);
    cy.contains('Send via QR Code').click();
    cy.contains('Send OTP').click();
  });
});
