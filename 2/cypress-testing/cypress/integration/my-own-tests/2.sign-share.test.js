/// <reference types="cypress" />

describe('Sign & Share Feature', () => {
  it.only('Sign & Share', () => {
    cy.visit('https://privy.id');

    cy.loginTest();

    cy.get('button#v-step-0').click();
    cy.contains('Sign & Request').click();

    const fileName = 'mrizki_appletter.pdf';
    cy.get('.form-upload > input[type="file"]').attachFile({
      filePath: fileName,
      encoding: 'base64',
    });

    cy.get('.modal-footer > .btn-danger').click();
    cy.wait(1000);
    cy.contains('Continue').click();
    cy.wait(1000);
    cy.get('#step-document-1').click();
    cy.contains('Done').click();
    cy.wait(4000);
    cy.contains('Send via QR Code').click();
    cy.contains('Send OTP').click();
    cy.wait(10000);
    cy.contains('Click to add recipient').click();
    cy.get('input[placeholder="Enter PrivyID"]').type('YX7397');
  });
});
